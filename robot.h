/** ============================================================================
 *  @file       robot.h
 *
 *  @brief      FirstPlace TIVA-C Robot Specific APIs
 *
 *  The FirstPlace TIVA-C Robot header file should be placed in project root folder and included in an application as
 *  follows:
 *  @code
 *  #include "robot.h"
 *  @endcode
 *
 *  ============================================================================
 *  Created on: OCT 14, 2018
 *      Author: Bach Nguyen & Binh Duong
 */

#ifndef __ROBOT_H_
#define __ROBOT_H_

#ifdef __cplusplus
extern "C" {
#endif

/*!
 *  @def    Robot State
 *  @brief  Enum of robot States  for TIVA-C FirstPlace robot
 */
typedef enum state {
    forward = 0,
    rightturn,
    turnback,
    stop,
    slow,

    State_COUNT
} state;

/*!
 *  @def    Robot task name
 *  @brief  Enum of robot task thread name for TIVA-C FirstPlace robot
 */
typedef enum ROBOT_TaskName {
    send = 0,
    pwmMotor,
    distance,
    PID,
    robotState,
    reflectance,
    blinky,

    ROBOT_TASKCOUNT
} ROBOT_TaskName;

/*!
 *  @def    ROBOT_UARTName
 *  @brief  Enum of UARTs on the FirstPlace TIVA-C robot
 */
typedef enum ROBOT_UARTName {
    ROBOT_UART_USB = 0,
    ROBOT_UART_PB0RX_PB1TX,

    ROBOT_UARTCOUNT
} ROBOT_UARTName;

/*!
 *  @def    ROBOT_PWMName
 *  @brief  Enum of PWM names on the FirstPlace TIVA-C robot
 */
typedef enum ROBOT_PWMName {
    ROBOT_PWM_PA6 = 0,
    ROBOT_PWM_PA7,

    ROBOT_PWMCOUNT
} ROBOT_PWMName;

/*!
 *  @def    ROBOT_GPIOName
 *  @brief  Enum of GPIO names on the FirstPlace TIVA-C robot
 */
typedef enum ROBOT_GPIOName {
    ROBOT_REF_PB2 = 0,
    ROBOT_REF_PE0,
    ROBOT_PHASE_PA5,
    ROBOT_PHASE_PB4,
    ROBOT_LED_RED,
    ROBOT_LED_BLUE,
    ROBOT_LED_GREEN,

    ROBOT_GPIOCOUNT
} ROBOT_GPIOName;

/*!
 *  @brief  Initialize the general board specific settings
 *
 *  This function initializes the general board specific settings.
 *  This includes:
 *     - Flash wait states based on the process
 *     - Disable clock source to watchdog module
 *     - Enable clock sources for peripherals
 */
extern void ROBOT_initGeneral(void);

/*!
 *  @brief  Initialize robot specific UART settings
 *
 *  This function initializes the board specific UART settings and then calls
 *  the UART_init API to initialize the UART module.
 *
 *  The UART peripherals controlled by the UART module are determined by the
 *  UART_config variable.
 */
extern void ROBOT_initUART(void);

/*!
 *  @brief  Initialize robot specific PWM settings
 *
 *  This function initializes the board specific PWM settings and then calls
 *  the PWM_init API to initialize the PWM module.
 *
 *  The PWM peripherals controlled by the PWM module are determined by the
 *  PWM_config variable.
 */
extern void ROBOT_initPWM(void);

/*!
 *  @brief  Initialize robot specific GPIO settings
 *
 *  This function initializes the board specific GPIO settings and
 *  then calls the GPIO_init API to initialize the GPIO module.
 *
 *  The GPIOs controlled by the GPIO module are determined by the GPIO_PinConfig
 *  variable.
 */
extern void ROBOT_initGPIO(void);

/*!
 *  @brief  Initialize robot specific ADC settings
 *
 *  This function Enable sample sequence 3 with a processor signal trigger.
 *  Sequence 3 will do a single sample when the processor sends a signal to start the
 *  conversion.
 *  Sample channel 0 (ADC_CTL_CH0) in single-ended mode
 */
extern void ROBOT_initADC(void);

/*!
 *  @brief  Read from ADC0 sequence 2 buffer
 *
 */
extern void ROBOT_ADC_sample(uint32_t *buf);

#endif

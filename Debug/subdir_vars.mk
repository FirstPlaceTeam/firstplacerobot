################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CFG_SRCS += \
../FirstPlace.cfg 

CMD_SRCS += \
../EK_TM4C123GXL.cmd 

C_SRCS += \
../FirstPlace.c \
../robot.c 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_MISC_DIRS += \
./configPkg/ 

C_DEPS += \
./FirstPlace.d \
./robot.d 

GEN_OPTS += \
./configPkg/compiler.opt 

OBJS += \
./FirstPlace.obj \
./robot.obj 

GEN_MISC_DIRS__QUOTED += \
"configPkg\" 

OBJS__QUOTED += \
"FirstPlace.obj" \
"robot.obj" 

C_DEPS__QUOTED += \
"FirstPlace.d" \
"robot.d" 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

C_SRCS__QUOTED += \
"../FirstPlace.c" \
"../robot.c" 



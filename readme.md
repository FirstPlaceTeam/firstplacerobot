Project was built with the purpose of learning RTOS and code execution analysis for real-time system.

Won first place in class competition.

Maze solving robot was built with TI Tiva-C launchpad (ARM-Cortex M4f) and components from Pololu.com

Distance sensors: values read in with ADC for PID control scheme to navigate the grids (wall follower, right hand rule).

Reflectance sensors: values determined by measuring capacitor discharge time, to detect black starting line and ending line.

Bluetooth module: used for receiving commands and sending various logged data back to the computer for PID tuning. 

Dual motor control driver: used in PWM driving/braking mode.

Tuning PID for max speed: https://youtu.be/v1Rx0NvSdcw

Users codes are limited to Firstplace.c, robot.c and robot.h

 *  ======== error2Buf ========
 *  Function to put error into pingpong buffer
 *  Called by distance task

 *  ======== sendTask ========
 *  Convert buffer floats to ASCII then send over bluetooth one by one
 *  Also toggle blue LED
 *  If stop condition is reached, turn off LEDs and write the stop time.
 *  Wait on sendSem, which is posted by PID task (error2Buf function)

 *  ======== distanceTask ========
 *  Sharp GP2Y0A41SK0F Analog Distance Sensor 4-30cm
 *  https://www.pololu.com/product/2464
 *  Read in ADC and convert values (not linear) to distance for ease PID processing
 *  Linearization of data based on datasheet formula 12/Voltage - 0.42, but with a few adjustment for more accurate result
 *  Task sleep will decide polling interval, and by holding semaphore, it ultimately decides how often PWM duties get adjusted
 
 *  ======== PIDTask ========
 *  Calculate PID gain Kpid
 *  Wait on pid semaphore
 
 *  ======== robotStateTask ========
 *  Decide motor speed based on current state
 *  Wait on state semaphore
 
 *  ======== pwmMotorTask ========
 *  DRV8835 Dual Motor Driver Carrier
 *  https://www.pololu.com/product/2135
 *  Motor driver is set on EN/Phase mode
 *  Initiate PWM on PA6 and PA7 and write the duty cycle according to given parameters.
 *  Wait on PWM semaphore

 *  ======== reflectanceTask ========
 *  QTR-1RC Reflectance Sensor
 *  https://www.pololu.com/product/2459
 *  First GPIO will be configure as output, write high.
 *  It take about 10usec for collector voltage to charge up to steady state, due to collector capacitor.
 *  Then GPIO will be configure as input, interrupt when low threshold detected.
 *  At the same time a timer starts counting.
 *  When interrupted, callback function will be called to record the time.
 *  Time for collector voltage to go from steady state high to low threshold depends on current through collector capacitor
 *  This current is modulated by photo transistor, proportional to infrared light reflected.
 *  With a strong reflectance, he decay time can be as low as several dozen microseconds;
 *  with no reflectance, the decay time can be up to a few milliseconds.

 *  ======== reflectanceHWI ========
 * Callback function
 * Triggered when reflectance GPIO reaches low input state
 * If threshold is reached, count the time seeing black
 * First time seeing black start clock tick
 * If seeing a lot of consecutive black, stop clock and stop robot.
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== FirstPlace.c ======== *
 *      Author: Bach Nguyen & Binh Duong
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/hal/Timer.h>
#include <ti/sysbios/knl/Clock.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
// #include <ti/drivers/I2C.h>
// #include <ti/drivers/SDSPI.h>
// #include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>
// #include <ti/drivers/WiFi.h>
#include <ti/drivers/PWM.h>

/* Robot Header file */
#include "robot.h"

/* Function Prototype */
Void reflectanceHWI(uint32_t GPIOindex);
void error2Buf(float error);

/* Define */
#define TASKSTACKSIZE   1024
#define FORWARD     0
#define REVERSE     1
#define pwmPeriod   1000
#define max_PWM     1000
#define reflThresh  1000
#define _100msec    100000

/* Global */
Task_Struct taskStruct[ROBOT_TASKCOUNT];
Char taskStack[TASKSTACKSIZE * ROBOT_TASKCOUNT];

UART_Handle USBSerial_uart, blueSmurf_uart;
Semaphore_Handle pwmSem, stateSem, pidSem, sendSem, reflSem, blinkySem;
Timer_Handle Timer[ROBOT_GPIOCOUNT];

state curState = stop;
uint32_t maxPWM = max_PWM,
         dutyPA6,               //Left motor
         dutyPA7,               //Right motor
         dirL, dirR;
uint32_t startTime = 0, stopTime = 0;        // Clock_getTick return int32 even though description says uint32
uint32_t bufCount;
float ping[20], pong[20], *pingpong = ping;
float   Kpid, Kp, Ki, Kd, brake, disRight, disFront;

/*
 *  ======== error2Buf ========
 *  Function to put error in to an array of float
 *  Called by distance task
 */
void error2Buf(float error)
{
    static int i,j = 0;
    i++;
    if (i == 5) {
        pingpong[j] = error;                            // put current error to buffer
        j++;
        bufCount = j;                                   // keeping track of how many are filled
        if (j == 20) {                                  // allow sending once full
            Semaphore_post(sendSem);
            j = 0;
        }
        i = 0;
    }
}

/*
 *  ======== sendTask ========
 *  Convert buffer floats to ASCII then send over bluetooth one by one
 *  Also toggle blue LED
 *  If stop condition is reached, turn off LEDs and write the stop time.
 *  Wait on sendSem, which is posted by PID task (error2Buf function)
 */
Void sendTask(UArg arg0, UArg arg1)
{
    uint32_t i, sendCount, charCount;
    float *sendBuf;
    char charBuf[50];

    /* Loop forever */
    while (1) {
        Semaphore_pend(sendSem, BIOS_WAIT_FOREVER);
        sendCount = bufCount;                                         // number of buffer cell filled
        sendBuf = pingpong;                                           // current buffer pointer

        /* Switching between ping and pong buffer */
        if (pingpong == ping) {
            pingpong = pong;
        } else {
            pingpong = ping;
        }
        Semaphore_post(blinkySem);

        for (i = 0; i < sendCount; i++) {                              // step the buffer one by one
            charCount = sprintf(charBuf, "\n%.2f", sendBuf[i]);        // convert float to string
            UART_write(blueSmurf_uart, charBuf, charCount);            // send string over bluetooth
        }

        if (stopTime > 0) {                                             // if semaphore was posted by the stop state after seeing stop line
            charCount = sprintf(charBuf, "\nStop time\t%d msec", stopTime);
            UART_write(blueSmurf_uart, charBuf, charCount);             // send stop time over bluetooth
        }
    }
}

/*
 *  ======== distanceTask ========
 *  Sharp GP2Y0A41SK0F Analog Distance Sensor 4-30cm
 *  https://www.pololu.com/product/2464
 *  Read in ADC and convert values (not linear) to distance for ease PID processing
 *  Linearization of data based on datasheet formula 12/Voltage - 0.42, but with a few adjustment for more accurate result
 *  Task sleep will decide polling interval, and by holding semaphore, it ultimately decides how often PWM duties get adjusted
 */
Void distanceTask(UArg arg0, UArg arg1)
{
    float volt_front, volt_side;
    uint32_t ADC_buf[2];

    /* Loop forever */
    while (1) {
        ROBOT_ADC_sample(ADC_buf);
        volt_front = 3.3/4096 * ADC_buf[0];                   // 12 bit ADC, PD1
        volt_side = 3.3/4096 * ADC_buf[1];                    // PD0
        disFront = 15/volt_front - 0.5;                       // linearize data into cm
        disRight = 15/volt_side - 0.5;                        // linearize data into cm
        Semaphore_post(pidSem);
        Task_sleep(20);                                       // Sensor updates every 15ms according to datasheet
    }
}

/*
 *  ======== PIDTask ========
 *  Calculate PID gain Kpid
 *  Wait on pid semaphore
 */
Void PIDTask(UArg arg0, UArg arg1)
{
    float ref_rightVal, P = 0, I = 0 , D = 0, error = 0, prev_error = 0;
    ref_rightVal = 11;
    /* Kp and Kd will be changed by user input */
    Kp = 0.2;                         // Kp * maxError < 1 to avoid jerking
    Ki = 0.005;
    Kd = 0.2;

    /* Loop forever */
    while(1){
        Semaphore_pend(pidSem, BIOS_WAIT_FOREVER);
        error = disRight - ref_rightVal;
        if (curState == forward) {
            P = error;
            if (Kpid < 0.5) {                           // anti wind up, I should not accumulate past saturation
                I += error;
            }
            D = error - prev_error;
            if (disRight < 20) {                        // ignore the error when seeing right turn
                error2Buf(error);                       // Send error to send buffer
            }
        } else {                                        // reset PID to 0 when turning or stop
            P = 0;
            I = 0;
            D = 0;
        }
        prev_error = error;

        Kpid = Kp*P + Ki*I + Kd*D;           // Gain Kpid should be less than 1 so to not saturate
        if (0){                              // braking not needed
            brake = 5/disFront;              // brake power is inversely proportional to the front wall
        } else {
            brake = 0;                       // no braking if no wall in front
        }
        Semaphore_post(stateSem);
    }
}

/*
 *  ======== robotStateTask ========
 *  Decide motor speed based on current state
 *  Wait on state semaphore
 */
Void robotStateTask(UArg arg0, UArg arg1)
{
    char readBuf[5];
    readBuf[4] = '\0';                 // string terminator

    /* Loop forever */
    while (1) {
        Semaphore_pend(stateSem, BIOS_WAIT_FOREVER);     // stateSem depends on pidSem, which is posted by distanceTask every 20msec
        switch (curState) {
        case forward:
            if (disFront < 8) {                         // sees wall, pause then turn back
                dutyPA6 = 0;
                dutyPA7 = 0;
                Semaphore_post(pwmSem);
                curState = turnback;
                break;
            } else if (disRight > 30) {                  // sees right path, keep going a bit more then turn right
                dutyPA6 = maxPWM;
                dutyPA7 = dutyPA6;
                Semaphore_post(pwmSem);
                Task_sleep(50);
                curState = rightturn;
                break;
            }

            dirL = FORWARD;
            dirR = FORWARD;
            if (Kpid >= 0) {                              // error to the left, lean right
                dutyPA6 = maxPWM*(1-brake);               // Left motor max
                dutyPA7 = dutyPA6*(1-Kpid);               // right motor < left
            } else {                                      // error to the right, lean left
                dutyPA7 = maxPWM*(1-brake);               // right motor max
                dutyPA6 = dutyPA7*(1+Kpid);               // Left motor < right
            }
            Semaphore_post(pwmSem);
            break;

        case rightturn:
            if (disRight < 13) {                          // sees wall to the right, go forward
                curState = forward;
                break;
            }

            dirL = FORWARD;
            dirR = FORWARD;
            dutyPA6 = pwmPeriod;                         // Left motor max
            dutyPA7 = dutyPA6*0.9/2;                    // right motor half
            Semaphore_post(pwmSem);
            break;

        case turnback:
            if (disFront > 20) {                         // sees path forward, pause slightly then move forward
                dutyPA6 = 0;
                dutyPA7 = 0;
                Semaphore_post(pwmSem);
                curState = forward;
//                Task_sleep(50);                          // pause slightly to avoid overshoot
                break;
            }

            dirL = REVERSE;                              // turn left direction to see if there a path to the left
            dirR = FORWARD;
            dutyPA6 = maxPWM;                            // left motor reverse
            dutyPA7 = dutyPA6;                           // right motor forward
            Semaphore_post(pwmSem);
            break;

        case stop:
            dutyPA6 = 0;                                 // left motor stop
            dutyPA7 = 0;                                 // right motor stop
            Semaphore_post(pwmSem);
            if (startTime == 0) {                        // at beginning state, block and wait for user input over bluetooth then start moving
                /* Tune PID value with user input */
                do {
                    UART_write(blueSmurf_uart, "\nKp\t", sizeof("\nKp\t"));
                    UART_read(blueSmurf_uart, &readBuf, 4);                               // read 4 characters (ex. 0.23)
                    Kp = (float)atof(readBuf);
                } while (Kp > 0.5 || Kp < 0);                                             // make sure Kp is a valid value

                do {
                    UART_write(blueSmurf_uart, "\nKd\t", sizeof("\nKd\t"));
                    UART_read(blueSmurf_uart, &readBuf, 4);
                    Kd = (float)atof(readBuf);                                            // read 4 characters (ex. 0.23)
                } while (Kd > 0.5 || Kd < 0);                                             // make sure Kd is a valid value

                do {
                    UART_write(blueSmurf_uart, "\nPress 'w' key\t", sizeof("\nPress 'w' key\t"));
                    UART_read(blueSmurf_uart, &readBuf, 1);        // read one character, only 'w' key can activate the robot, other keys do nothing
                } while (readBuf[0] != 'w');
                curState = forward;
                UART_write(blueSmurf_uart, "\nStarting", sizeof("\nStarting"));
                GPIO_write(ROBOT_LED_GREEN, 1);          // turn on green led
                startTime = Clock_getTicks();            // mark start clock tick

            } else if (stopTime > 0) {                   // if reached the end of the run
                Task_sleep(100);
                GPIO_write(ROBOT_LED_GREEN, 0);          // turn off green led
                Task_sleep(5000);                       // pause long time waiting to be picked up
                startTime = 0;                           // reset to beginning state
                stopTime = 0;
            }
            Task_sleep(100);
            break;
        }
    }
}

/*
 *  ======== pwmMotorTask ========
 *  DRV8835 Dual Motor Driver Carrier
 *  https://www.pololu.com/product/2135
 *  Motor driver is set on EN/Phase mode
 *  Initiate PWM on PA6 and PA7 and write the duty cycle according to given parameters.
 *  Wait on PWM semaphore
 */
Void pwmMotorTask(UArg arg0, UArg arg1)
{
    PWM_Handle pwmPA6;
    PWM_Handle pwmPA7;
    PWM_Params pwmParams;

    PWM_Params_init(&pwmParams);
    pwmParams.period = pwmPeriod;
    pwmPA6 = PWM_open(ROBOT_PWM_PA6, &pwmParams);
    pwmPA7 = PWM_open(ROBOT_PWM_PA7, &pwmParams);
    if (pwmPA6 == NULL || pwmPA7 == NULL) {
        System_abort("One or both PWM did not open");
    }

    /* Loop forever */
    while (1) {
        Semaphore_pend(pwmSem, BIOS_WAIT_FOREVER);
        GPIO_write(ROBOT_PHASE_PA5, dirL);
        GPIO_write(ROBOT_PHASE_PB4, dirR);
        PWM_setDuty(pwmPA6, dutyPA6);
        PWM_setDuty(pwmPA7, dutyPA7);
    }
}

/*
 *  ======== reflectanceTask ========
 *  QTR-1RC Reflectance Sensor
 *  https://www.pololu.com/product/2459
 *  First GPIO will be configure as output, write high.
 *  It take about 10usec for collector voltage to charge up to steady state, due to collector capacitor.
 *  Then GPIO will be configure as input, interrupt when low threshold detected.
 *  At the same time a timer starts counting.
 *  When interrupted, callback function will be called to record the time.
 *  Time for collector voltage to go from steady state high to low threshold depends on current through collector capacitor
 *  This current is modulated by photo transistor, proportional to infrared light reflected.
 *  With a strong reflectance, he decay time can be as low as several dozen microseconds;
 *  with no reflectance, the decay time can be up to a few milliseconds.
 */
Void reflectanceTask(UArg arg0, UArg arg1)
{
    Timer_Params timerParams;
    Timer_Params_init(&timerParams);
    timerParams.period = _100msec;                                           // usec. Plenty of time for cap to discharge completely
    timerParams.startMode = Timer_StartMode_USER;
    timerParams.runMode = Timer_RunMode_ONESHOT;
    Timer[ROBOT_REF_PB2] = Timer_create(4, NULL, &timerParams, NULL);       // Any timer ID less than 4 or Timer_ANY cause abort, suspecting TIRTOS bug
    Timer[ROBOT_REF_PE0] = Timer_create(5, NULL, &timerParams, NULL);
    if (Timer[ROBOT_REF_PB2] == NULL || Timer[ROBOT_REF_PE0] == NULL) {
        System_abort("Timer create failed");
    }
    GPIO_setCallback(ROBOT_REF_PB2, (GPIO_CallbackFxn)reflectanceHWI);
    GPIO_setCallback(ROBOT_REF_PE0, (GPIO_CallbackFxn)reflectanceHWI);

    /* Loop forever */
    while (1) {
        GPIO_setConfig(ROBOT_REF_PB2, GPIO_CFG_OUTPUT | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_HIGH);         // set GPIO as ouput high to charge capacitor
        GPIO_setConfig(ROBOT_REF_PE0, GPIO_CFG_OUTPUT | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_HIGH);
        Task_sleep(1);                                                  // Only need a few usec
        GPIO_enableInt(ROBOT_REF_PB2);
        GPIO_setConfig(ROBOT_REF_PB2, GPIO_CFG_INPUT | GPIO_CFG_IN_INT_LOW);                                // set GPIO as input, let capacitor discharge through light transitor
        Timer_start(Timer[ROBOT_REF_PB2]);                                                                  // start timer for discharge time, interrupt when input goes low
        GPIO_enableInt(ROBOT_REF_PE0);
        GPIO_setConfig(ROBOT_REF_PE0, GPIO_CFG_INPUT | GPIO_CFG_IN_INT_LOW);
        Timer_start(Timer[ROBOT_REF_PE0]);
        Task_sleep(10);                                                                                     // for more predictable reading period
        Semaphore_pend(reflSem, BIOS_WAIT_FOREVER);                                                         // wait until reflectanceHWI activated before charging the cap again
    }
}

/*
 *  ======== reflectanceHWI ========
 * Callback function
 * Triggered when reflectance GPIO reaches low input state
 * If threshold is reached, count the time seeing black
 * First time seeing black start clock tick
 * If seeing a lot of consecutive black, stop clock and stop robot.
 */
Void reflectanceHWI(uint32_t GPIOindex)
{
    static int i = 0;
    int32_t dischargeTime = 0;

    GPIO_disableInt(GPIOindex);                          // make sure no repeating interrupts
    Timer_stop(Timer[GPIOindex]);
    dischargeTime = _100msec - Timer_getCount(Timer[GPIOindex])/80; // 80MHz => 1tick = 1/80usec
    Semaphore_post(reflSem);                             // let GPIO charge the cap again
    if (dischargeTime > reflThresh && startTime > 0) {                    // detect black line
//        maxPWM = pwmPeriod/2;                             // use fixed speed
        i++;                                             // start counting black line
//        if ( startTime == 0) {                          // only once when first sees black
//            startTime = Clock_getTicks();               // mark start clock tick
//            GPIO_write(ROBOT_LED_GREEN, 1);             // turn on green led
//        }
        if (i > 20 && stopTime == 0) {                   // detect thick black line while running
            stopTime = Clock_getTicks();                 // mark end clock tick
            stopTime = stopTime - startTime;             // total clock tick, 1 tick = 1msec
            Semaphore_post(sendSem);                     // allow send over bluetooth the remaining last error buffer and the stop time
            curState = stop;
            Semaphore_post(stateSem);
        }
    } else if (i > 0) {                                  // if sees white again
//        maxPWM = max_PWM;                               // run full speed
        i = 0;                                           // restart black line count
    }
}

/*
 *  ======== blinkyTask ========
 *  To blink Blue led 3 times in half a second. Pending on blinkySem
 */
Void blinkyTask(UArg arg0, UArg arg1) {
    int i;
    while (1) {
        Semaphore_pend(blinkySem, BIOS_WAIT_FOREVER);
        for (i = 0; i < 6; i++) {
            GPIO_toggle(ROBOT_LED_BLUE);
            Task_sleep(100);
        }
    }
}

/*
 *  ======== main ========
 */
int main(void)
{
    /* Call board init functions */
    ROBOT_initGeneral();
    ROBOT_initGPIO();
    ROBOT_initPWM();
    ROBOT_initUART();
    ROBOT_initADC();

    Semaphore_Params semParams;
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    pidSem = Semaphore_create(0, &semParams, NULL);
    stateSem = Semaphore_create(0, &semParams, NULL);
    pwmSem = Semaphore_create(0, &semParams, NULL);
	sendSem = Semaphore_create(0, &semParams, NULL);
	blinkySem = Semaphore_create(0,&semParams, NULL);
	reflSem = Semaphore_create(0, &semParams, NULL);
    if (pwmSem == NULL || stateSem == NULL || pidSem == NULL || sendSem == NULL || reflSem == NULL) {
        System_abort("Error creating the Semaphores");
    }

    UART_Params uartParams;
    UART_Params_init(&uartParams);
    USBSerial_uart = UART_open(ROBOT_UART_USB, &uartParams);
    blueSmurf_uart = UART_open(ROBOT_UART_PB0RX_PB1TX, &uartParams);
    if (USBSerial_uart == NULL || blueSmurf_uart == NULL) {
        System_abort("Error opening the UART");
    }

    Task_Params taskParams[ROBOT_TASKCOUNT];
    /* Create send task thread */
    Task_Params_init(&taskParams[send]);
    taskParams[send].stackSize = TASKSTACKSIZE;
    taskParams[send].stack = taskStack + (TASKSTACKSIZE * send);
    taskParams[send].instance->name = "send";
    Task_construct(&taskStruct[send], (Task_FuncPtr)sendTask, &taskParams[send], NULL);

    /* Create pwmMotor task thread */
    Task_Params_init(&taskParams[pwmMotor]);
    taskParams[pwmMotor].stackSize = TASKSTACKSIZE;
    taskParams[pwmMotor].stack = taskStack + (TASKSTACKSIZE * pwmMotor);
    taskParams[pwmMotor].instance->name = "pwmMotor";
    Task_construct(&taskStruct[pwmMotor], (Task_FuncPtr)pwmMotorTask, &taskParams[pwmMotor], NULL);

    /* Create Distance task thread */
    Task_Params_init(&taskParams[distance]);               // initialize default Task parameters
    taskParams[distance].stackSize = TASKSTACKSIZE;
    taskParams[distance].stack = taskStack + (TASKSTACKSIZE * distance);
    taskParams[distance].instance->name = "distance";
    Task_construct(&taskStruct[distance], (Task_FuncPtr)distanceTask, &taskParams[distance], NULL);

    /* Create PID task thread */
    Task_Params_init(&taskParams[PID]);               // initialize default Task parameters
    taskParams[PID].stackSize = TASKSTACKSIZE;
    taskParams[PID].stack = taskStack + (TASKSTACKSIZE * PID);
    taskParams[PID].instance->name = "PID";
    Task_construct(&taskStruct[PID], (Task_FuncPtr)PIDTask, &taskParams[PID], NULL);

    /* Create robotState task thread */
    Task_Params_init(&taskParams[robotState]);               // initialize default Task parameters
    taskParams[robotState].stackSize = TASKSTACKSIZE;
    taskParams[robotState].stack = taskStack + (TASKSTACKSIZE * robotState);
    taskParams[robotState].instance->name = "robotState";
    Task_construct(&taskStruct[robotState], (Task_FuncPtr)robotStateTask, &taskParams[robotState], NULL);

    /* Create reflectance task thread */
    Task_Params_init(&taskParams[reflectance]);               // initialize default Task parameters
    taskParams[reflectance].stackSize = TASKSTACKSIZE;
    taskParams[reflectance].stack = taskStack + (TASKSTACKSIZE * reflectance);
    taskParams[reflectance].instance->name = "reflectance";
    Task_construct(&taskStruct[reflectance], (Task_FuncPtr)reflectanceTask, &taskParams[reflectance], NULL);

    /* Create blinky task thread */
    Task_Params_init(&taskParams[blinky]);               // initialize default Task parameters
    taskParams[blinky].stackSize = TASKSTACKSIZE;
    taskParams[blinky].stack = taskStack + (TASKSTACKSIZE * blinky);
    taskParams[blinky].instance->name = "blinky";
    Task_construct(&taskStruct[blinky], (Task_FuncPtr)blinkyTask, &taskParams[blinky], NULL);

    /* Start TI_RTOS */
    BIOS_start();

    return (0);
}

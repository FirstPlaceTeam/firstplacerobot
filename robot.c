/*
 *  ======== robot.c ========
 *  This file is an re-implementation of some sections from EK_TM4C123GXL.c
 *  for setting up the robot specific items for the FirstPlace TIVA-C robot project.
 *      Author: Bach Nguyen & Binh Duong
 */

#include <stdint.h>
#include <stdbool.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/family/arm/m3/Hwi.h>

#include <inc/hw_ints.h>
#include <inc/hw_memmap.h>
#include <inc/hw_types.h>
#include <inc/hw_gpio.h>

#include <driverlib/gpio.h>
#include <driverlib/i2c.h>
#include <driverlib/pin_map.h>
#include <driverlib/pwm.h>
#include <driverlib/ssi.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>
#include <driverlib/udma.h>
#include <driverlib/adc.h>

#include "robot.h"

/*
 *  =============================== General ===============================
 */
/*
 *  ======== ROBOT_initGeneral ========
 */
void ROBOT_initGeneral(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
}

/*
 *  ======== UART ========
 *  This section re-implement the UART section on EK_TM4C123GXL.c to allow additional UART
 *  UART DMA functionality is omitted for simplicity
 */

#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_SECTION(UART_config, ".const:UART_config")
#pragma DATA_SECTION(uartRobotHWAttrs, ".const:uartRobotHWAttrs")
#endif

#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTTiva.h>

UARTTiva_Object uartRobotObjects[ROBOT_UARTCOUNT];
unsigned char uartRobotRingBuffer[ROBOT_UARTCOUNT][32];

/* UART configuration structure */
const UARTTiva_HWAttrs uartRobotHWAttrs[ROBOT_UARTCOUNT] = {
    {
        .baseAddr = UART0_BASE,
        .intNum = INT_UART0,
        .intPriority = (~0),
        .flowControl = UART_FLOWCONTROL_NONE,
        .ringBufPtr  = uartRobotRingBuffer[ROBOT_UART_USB],
        .ringBufSize = sizeof(uartRobotRingBuffer[ROBOT_UART_USB])
    },
    {
        .baseAddr = UART1_BASE,
        .intNum = INT_UART1,
        .intPriority = (~0),
        .flowControl = UART_FLOWCONTROL_NONE,
        .ringBufPtr  = uartRobotRingBuffer[ROBOT_UART_PB0RX_PB1TX],
        .ringBufSize = sizeof(uartRobotRingBuffer[ROBOT_UART_PB0RX_PB1TX])
    }
};

const UART_Config UART_config[] = {
    {
        .fxnTablePtr = &UARTTiva_fxnTable,
        .object = &uartRobotObjects[ROBOT_UART_USB],
        .hwAttrs = &uartRobotHWAttrs[ROBOT_UART_USB]
    },
    {
        .fxnTablePtr = &UARTTiva_fxnTable,
        .object = &uartRobotObjects[ROBOT_UART_PB0RX_PB1TX],
        .hwAttrs = &uartRobotHWAttrs[ROBOT_UART_PB0RX_PB1TX]
    },
    {NULL, NULL, NULL}
};

/*
 *  ======== ROBOT_initUART ========
 */
void ROBOT_initUART(void)
{
    /* Enable and configure the peripherals used by the uart. */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
    GPIOPinConfigure(GPIO_PB0_U1RX);
    GPIOPinConfigure(GPIO_PB1_U1TX);
    GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    /* Initialize the UART driver */
    UART_init();
}

/*
 *  =============================== PWM ===============================
 *  This section is the same as on EK_TM4C123GXL.c */
/* Place into subsections to allow the TI linker to remove items properly */
#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_SECTION(PWM_config, ".const:PWM_config")
#pragma DATA_SECTION(pwmTivaHWAttrs, ".const:pwmTivaHWAttrs")
#endif

#include <ti/drivers/PWM.h>
#include <ti/drivers/pwm/PWMTiva.h>

PWMTiva_Object pwmTivaObjects[ROBOT_PWMCOUNT];

const PWMTiva_HWAttrs pwmTivaHWAttrs[ROBOT_PWMCOUNT] = {
    {
        .baseAddr = PWM1_BASE,
        .pwmOutput = PWM_OUT_2,
        .pwmGenOpts = PWM_GEN_MODE_DOWN | PWM_GEN_MODE_DBG_RUN
    },
    {
        .baseAddr = PWM1_BASE,
        .pwmOutput = PWM_OUT_3,
        .pwmGenOpts = PWM_GEN_MODE_DOWN | PWM_GEN_MODE_DBG_RUN
    }
};

const PWM_Config PWM_config[] = {
    {
        .fxnTablePtr = &PWMTiva_fxnTable,
        .object = &pwmTivaObjects[0],
        .hwAttrs = &pwmTivaHWAttrs[0]
    },
    {
        .fxnTablePtr = &PWMTiva_fxnTable,
        .object = &pwmTivaObjects[1],
        .hwAttrs = &pwmTivaHWAttrs[1]
    },
    {NULL, NULL, NULL}
};

/*
 *  ======== ROBOT_initPWM ========
 */
void ROBOT_initPWM(void)
{
    /* Enable PWM peripherals */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);

    /* Enable PWM output on GPIO pins */
    GPIOPinConfigure(GPIO_PA6_M1PWM2);
    GPIOPinConfigure(GPIO_PA7_M1PWM3);
    GPIOPinTypePWM(GPIO_PORTA_BASE, GPIO_PIN_6 |GPIO_PIN_7);

    PWM_init();
}

/*
 *  =============================== GPIO ===============================
 *  This section re-implement the GPIO section on EK_TM4C123GXL.c to allow additional GPIO
 */
/* Place into subsections to allow the TI linker to remove items properly */
#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_SECTION(GPIOTiva_config, ".const:GPIOTiva_config")
#endif

#include <ti/drivers/GPIO.h>
#include <ti/drivers/gpio/GPIOTiva.h>

/*
 * Array of Pin configurations
 * NOTE: The order of the pin configurations must coincide with what was
 *       defined in Robot.h
 * NOTE: Pins not used for interrupts should be placed at the end of the
 *       array.  Callback entries can be omitted from callbacks array to
 *       reduce memory usage.
 */
GPIO_PinConfig gpioPinConfigs[] = {
    /* Pins for runtime configuring */
    /* PB2 */
    GPIOTiva_PB_2 | GPIO_DO_NOT_CONFIG,
    /* PE0 */
    GPIOTiva_PE_0 | GPIO_DO_NOT_CONFIG,

    /* Input pins */


    /* Output pins */
    /* PA5 */
    GPIOTiva_PA_5 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
    /* PB4 */
    GPIOTiva_PB_4 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
    /* EK_TM4C123GXL_LED_RED */
    GPIOTiva_PF_1 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
    /* EK_TM4C123GXL_LED_BLUE */
    GPIOTiva_PF_2 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
    /* EK_TM4C123GXL_LED_GREEN */
    GPIOTiva_PF_3 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW
};

/*
 * Array of callback function pointers
 * NOTE: The order of the pin configurations must coincide with what was
 *       defined in EK_TM4C123GXL.h
 * NOTE: Pins not used for interrupts can be omitted from callbacks array to
 *       reduce memory usage (if placed at end of gpioPinConfigs array).
 */
GPIO_CallbackFxn gpioCallbackFunctions[] = {
    NULL,
    NULL
};

/* The device-specific GPIO_config structure */
const GPIOTiva_Config GPIOTiva_config = {
    .pinConfigs = (GPIO_PinConfig *)gpioPinConfigs,
    .callbacks = (GPIO_CallbackFxn *)gpioCallbackFunctions,
    .numberOfPinConfigs = sizeof(gpioPinConfigs)/sizeof(GPIO_PinConfig),
    .numberOfCallbacks = sizeof(gpioCallbackFunctions)/sizeof(GPIO_CallbackFxn),
    .intPriority = (~0)
};

/*
 *  ======== ROBOT_initGPIO ========
 */
void ROBOT_initGPIO(void)
{
    /* Initialize peripheral and pins */
    GPIO_init();
}

/*
 *  ======== ROBOT_ADC_init ========
 */
void ROBOT_initADC(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_1);                // AIN6
    GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_0);                // AIN7
    /*
     * PB6 is shorted with PD0 and PB7 shorted with PD1
     * PB6 and PB7 need to be brought into a GPIO input state
     * so they don't interfere PD0 and PD1 operation.
     */
    GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_6);
    GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_7);
    /*
     * Enable sample sequence 2 with a processor signal trigger.  Sequence 2
     * will take 4 samples when the processor sends a signal to start the
     * conversion.  Each ADC module has 4 programmable sequences, sequence 0
     * to sequence 3.
    */
    ADCSequenceConfigure(ADC0_BASE, 2, ADC_TRIGGER_PROCESSOR, 0);
    /*
     * Configure step x on sequence 2.  Sample channel AINx (ADC_CTL_CH0) in
     * single-ended mode (default) and configure the interrupt flag
     * (ADC_CTL_IE) to be set when the sample is done.  Tell the ADC logic
     * that this is the last conversion (ADC_CTL_END).  Sequence
     * 3 has only one programmable step.  Sequence 1 and 2 have 4 steps, and
     * sequence 0 has 8 programmable steps
    */
    ADCSequenceStepConfigure(ADC0_BASE, 2, 0, ADC_CTL_CH6);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 1, ADC_CTL_IE | ADC_CTL_END | ADC_CTL_CH7);
    ADCSequenceEnable(ADC0_BASE, 2);
}

/*
 *  ======== ROBOT_ADC_sample ========
 *  Please be aware that this driver use Tivaware driverlib directly
 *  which is not built to be thread-safe for TI-RTOS
 *  Thus, it should only be used in a linear fashion (only one task can call it)
 */
void ROBOT_ADC_sample(uint32_t *buf)
{
    ADCIntClear(ADC0_BASE, 2);
    ADCProcessorTrigger(ADC0_BASE, 2);
    while(!ADCIntStatus(ADC0_BASE, 2, false)) {}
    ADCSequenceDataGet(ADC0_BASE, 2, buf);
}

